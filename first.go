package main

import "net/http"

func Handle(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("Hello AITU students!"))
}
func Handle2(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("goodbye AITU students!"))
}

func main() {
	http.HandleFunc("/aitu", Handle)
	http.HandleFunc("/bye", Handle2)
	http.ListenAndServe(":8080", nil)

}
